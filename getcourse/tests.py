from django.test import TestCase
from unittest.mock import patch
from getcourse.models import User, Deal
from getcourse.getcourse_import import *


class FillUserParamsTestCase(TestCase):
    """Тесты для функции fill_user_params"""
    def setUp(self):
        self.user_all_fields = User(
            email='test@example.com',
            phone='+1234567890',
            name='John',
            surname='Doe',
            city='New York',
            country='USA')
        self.user_missing_fields = User(
            email='test@example.com',
            surname='Doe')
        self.user_empty_fields = User(
            email='',
            phone='',
            name='',
            surname='',
            city='',
            country='')

    def test_fill_user_params_all_fields(self):
        """Проверка заполнения всех параметров при наличии всех данных"""
        result = fill_user_params(self.user_all_fields)

        self.assertEqual(result, {
            'email': self.user_all_fields.email,
            'phone': self.user_all_fields.phone,
            'first_name': self.user_all_fields.name,
            'last_name': self.user_all_fields.surname,
            'city': self.user_all_fields.city,
            'country': self.user_all_fields.country
        })
        self.assertEqual(len(result), 6)

    def test_fill_user_params_missing_fields(self):
        """Проверка заполнения параметров при наличии не всех данных"""
        result = fill_user_params(self.user_missing_fields)

        self.assertEqual(result, {
            'email': self.user_missing_fields.email,
            'last_name': self.user_missing_fields.surname
        })
        self.assertEqual(len(result), 2)

    def test_fill_user_params_empty_fields(self):
        """Проверка отработки заполнения параметров при отсутствии значений"""
        result = fill_user_params(self.user_empty_fields)

        self.assertEqual(result, {})

    def test_fill_user_params_invalid_input(self):
        """Проверка на исключение при некорректных входных данных"""
        with self.assertRaises(AttributeError):
            fill_user_params('invalid_input')


class FillDealParamsTestCase(TestCase):
    """Тесты для функции fill_deal_params"""
    def setUp(self):
        self.deal_all_fields = Deal.objects.create(
            deal_id='1',
            deal_number='12345',
            user_id='1',
            user_name='John Doe',
            user_email='johndoe@example.com',
            user_phone='1234567890',
            creat_date='2022-03-01 10:00:00',
            payment_date='2022-03-02 10:00:00',
            product_title='Test product',
            deal_status='new',
            price='1000',
            paid='Yes',
            payment_system_commission='10',
            received='900',
            tax='100',
            remaining_after_deduction='800',
            other_fees='0',
            earned='400',
            currency='RUB',
            manager='manager@example.com',
            city='New York',
            payment_system='Visa',
            partner_id='partner1',
            order_partner_email='partner@example.com')
        self.deal_with_missing_fields = Deal.objects.create(
            deal_id='test_deal_id',
            deal_number='test_deal_number',
            user_id='test_user_id',
            user_name='test_user_name',
            paid='NO'
        )

    def test_deal_params(self):
        """Проверка заполнения параметров сделки со всеми параметрами"""
        result = fill_deal_params(self.deal_all_fields)

        self.assertEqual(result, {
            'deal_number': self.deal_all_fields.deal_id,
            'offer_code': self.deal_all_fields.deal_number,
            'product_title': self.deal_all_fields.product_title,
            'deal_cost': self.deal_all_fields.price,
            'deal_status': self.deal_all_fields.deal_status,
            'deal_is_paid': self.deal_all_fields.paid,
            'manager_email': self.deal_all_fields.manager,
            'deal_created_at': self.deal_all_fields.creat_date,
            'deal_finished_at': self.deal_all_fields.payment_date,
            'partner_email': self.deal_all_fields.order_partner_email,
            'deal_currency': self.deal_all_fields.currency
            })
        self.assertEqual(len(result), 11)

    def test_deal_with_missing_fields(self):
        """Проверка заполнения параметров при наличии не всех данных"""
        result = fill_deal_params(self.deal_with_missing_fields)

        self.assertEqual(result, {
            'deal_number': self.deal_with_missing_fields.deal_id,
            'offer_code': self.deal_with_missing_fields.deal_number,
            'deal_is_paid': self.deal_with_missing_fields.paid
            })
        self.assertEqual(len(result), 3)

    def test_fill_user_params_invalid_input(self):
        """Проверка на исключение при некорректных входных данных"""
        with self.assertRaises(AttributeError):
            fill_deal_params('invalid_input')


class FillUserSystemParamsTest(TestCase):
    """Тесты для функции fill_user_system_params"""
    def setUp(self):
        self.user_has_partner_email = User.objects.create(
            user_id=1,
            email='test@test.com',
            partner_email='partner@test.com')
        self.user_does_not_have_partner_email = User.objects.create(user_id=2)

    def test_fill_user_system_params_with_partner_email(self):
        """Положительный тест, когда у пользователя есть partner_email"""
        result = fill_user_system_params(self.user_has_partner_email)

        self.assertEqual(result, {'refresh_if_exists': 1, 'partner_email': self.user_has_partner_email.partner_email})
        self.assertEqual(len(result), 2)

    def test_fill_user_system_params_without_partner_email(self):
        """Положительный тест, когда у пользователя нет partner_email"""
        result = fill_user_system_params(self.user_does_not_have_partner_email)

        self.assertEqual(result, {'refresh_if_exists': 1})
        self.assertEqual(len(result), 1)

    def test_fill_user_system_params_with_AttributeError(self):
        """Проверка на исключение при некорректных входных данных"""
        with self.assertRaises(AttributeError):
            fill_user_system_params('invalid_input')


class FillDealSystemParams(TestCase):
    """Тесты для функции fill_deal_system_params"""
    def setUp(self):
        self.user_has_partner_email = User.objects.create(
            user_id=1,
            email='test@test.com',
            partner_email='partner@test.com')
        self.user_does_not_have_partner_email = User.objects.create(user_id=2)

    def test_fill_deal_system_params_with_partner_email(self):
        """Положительный тест, когда у пользователя есть partner_email"""
        result = fill_deal_system_params(self.user_has_partner_email)

        self.assertEqual(result, {
            'refresh_if_exists': 1,
            'partner_email': self.user_has_partner_email.partner_email,
            'multiple_offers': 0,
            'return_payment_link': 0,
            'return_deal_number': 0
        })
        self.assertEqual(len(result), 5)

    def test_fill_deal_system_params_without_partner_email(self):
        """Положительный тест, когда у пользователя нет partner_email"""
        result = fill_deal_system_params(self.user_does_not_have_partner_email)

        self.assertEqual(result, {
            'refresh_if_exists': 1,
            'multiple_offers': 0,
            'return_payment_link': 0,
            'return_deal_number': 0
        })
        self.assertEqual(len(result), 4)

    def test_fill_deal_system_params_with_AttributeError(self):
        """Проверка на исключение при некорректных входных данных"""
        with self.assertRaises(AttributeError):
            fill_deal_system_params('invalid_input')


class FillSessionParams(TestCase):
    """Тесты для функции fill_session_params"""
    def setUp(self):
        self.user_with_utm = User.objects.create(
            user_id=1,
            utm_source='google',
            utm_medium='cpc',
            utm_campaign='test_campaign',
            utm_term='test_term',
            utm_content='test_content')
        self.user_does_not_have_partner_email = User.objects.create(user_id=2)

    def test_fill_deal_system_params_with_partner_email(self):
        """Положительный тест, когда у пользователя заполнены utm параметры"""
        result = fill_session_params(self.user_with_utm)

        self.assertEqual(result, {
            'utm_source': self.user_with_utm.utm_source,
            'utm_medium': self.user_with_utm.utm_medium,
            'utm_campaign': self.user_with_utm.utm_campaign,
            'utm_term': self.user_with_utm.utm_term,
            'utm_content': self.user_with_utm.utm_content
        })
        self.assertEqual(len(result), 5)

    def test_fill_deal_system_params_without_partner_email(self):
        """Положительный тест, когда у пользователя нет utm параметров"""
        result = fill_session_params(self.user_does_not_have_partner_email)

        self.assertEqual(len(result), 0)

    def test_fill_deal_system_params_with_AttributeError(self):
        """Проверка на исключение при некорректных входных данных"""
        with self.assertRaises(AttributeError):
            fill_session_params('invalid_input')


class CreateParams(TestCase):
    """Тесты для функции CreateParams"""
    def setUp(self):
        self.user_all_fields = User(
            user_id=1,
            email='test@example.com',
            phone='+1234567890',
            name='John',
            surname='Doe',
            city='New York',
            country='USA',
            utm_source='google',
            utm_medium='cpc',
            utm_campaign='test_campaign',
            utm_term='test_term',
            utm_content='test_content',
            partner_email='test@part.ru')
        self.user_missing_fields = User(
            user_id=2,
            email='test@example.com')
        self.deal_all_fields = Deal.objects.create(
            deal_id='1',
            deal_number='12345',
            user_id='1',
            creat_date='2022-03-01 10:00:00',
            payment_date='2022-03-02 10:00:00',
            product_title='Test product',
            deal_status='new',
            price='1000',
            paid='Yes',
            currency='RUB',
            manager='manager@example.com',
            city='New York',
            payment_system='Visa',
            partner_id='partner1',
            order_partner_email='partner@example.com')
        self.deal_missing_fields = Deal(
            deal_id='2',
            deal_number='2',
            paid='No')

    def test_create_params_for_users_method(self):
        """Тесты параметров импорта пользователя со всеми данными"""
        result = create_params('users', self.user_all_fields)

        self.assertEqual(result, {
            'user': {
                'email': self.user_all_fields.email,
                'phone': self.user_all_fields.phone,
                'first_name': self.user_all_fields.name,
                'last_name': self.user_all_fields.surname,
                'city': self.user_all_fields.city,
                'country': self.user_all_fields.country
            },
            'system': {
                'refresh_if_exists': 1,
                'partner_email': self.user_all_fields.partner_email
            },
            'session': {
                'utm_source': self.user_all_fields.utm_source,
                'utm_medium': self.user_all_fields.utm_medium,
                'utm_campaign': self.user_all_fields.utm_campaign,
                'utm_term': self.user_all_fields.utm_term,
                'utm_content': self.user_all_fields.utm_content
            }})
        self.assertEqual(len(result), 3)

    def test_create_params_for_users_method_missing_fields(self):
        """Тесты параметров импорта пользователя без данных"""
        result = create_params('users', self.user_missing_fields)

        self.assertEqual(result, {
            'user': {
                'email': self.user_missing_fields.email
            },
            'system': {
                'refresh_if_exists': 1
            },
            'session': {}})
        self.assertEqual(len(result), 3)

    def test_create_params_for_deals_method(self):
        """Тесты параметров импорта заказа со всеми данными"""
        result = create_params('deals', self.user_all_fields, self.deal_all_fields)

        self.assertEqual(result, {
            'user': {
                'email': self.user_all_fields.email,
                'phone': self.user_all_fields.phone,
                'first_name': self.user_all_fields.name,
                'last_name': self.user_all_fields.surname,
                'city': self.user_all_fields.city,
                'country': self.user_all_fields.country
            },
            'system': {
                'refresh_if_exists': 1,
                'partner_email': self.user_all_fields.partner_email,
                'multiple_offers': 0,
                'return_payment_link': 0,
                'return_deal_number': 0
            },
            'session': {
                'refresh_if_exists': 1,
                'partner_email': self.user_all_fields.partner_email
            },
            'deal': {
                'deal_number': self.deal_all_fields.deal_id,
                'offer_code': self.deal_all_fields.deal_number,
                'product_title': self.deal_all_fields.product_title,
                'deal_cost': self.deal_all_fields.price,
                'deal_status': self.deal_all_fields.deal_status,
                'deal_is_paid': self.deal_all_fields.paid,
                'manager_email': self.deal_all_fields.manager,
                'deal_created_at': self.deal_all_fields.creat_date,
                'deal_finished_at': self.deal_all_fields.payment_date,
                'partner_email': self.deal_all_fields.order_partner_email,
                'deal_currency': self.deal_all_fields.currency
            }
        })
        self.assertEqual(len(result), 4)

    def test_create_params_for_deals_method_missing_user_fields(self):
        """Тесты параметров импорта заказа с неполными данными пользователя"""
        result = create_params('deals', self.user_missing_fields, self.deal_all_fields)

        self.assertEqual(result, {
            'user': {
                'email': self.user_missing_fields.email
            },
            'system': {
                'refresh_if_exists': 1,
                'multiple_offers': 0,
                'return_payment_link': 0,
                'return_deal_number': 0
            },
            'session': {
                'refresh_if_exists': 1
            },
            'deal': {
                'deal_number': self.deal_all_fields.deal_id,
                'offer_code': self.deal_all_fields.deal_number,
                'product_title': self.deal_all_fields.product_title,
                'deal_cost': self.deal_all_fields.price,
                'deal_status': self.deal_all_fields.deal_status,
                'deal_is_paid': self.deal_all_fields.paid,
                'manager_email': self.deal_all_fields.manager,
                'deal_created_at': self.deal_all_fields.creat_date,
                'deal_finished_at': self.deal_all_fields.payment_date,
                'partner_email': self.deal_all_fields.order_partner_email,
                'deal_currency': self.deal_all_fields.currency
            }
        })
        self.assertEqual(len(result), 4)

    def test_create_params_for_deals_method_missing_deal_fields(self):
        """Тесты параметров импорта заказа с неполными данными заказа"""
        result = create_params('deals', self.user_all_fields, self.deal_missing_fields)

        self.assertEqual(result, {
            'user': {
                'email': self.user_all_fields.email,
                'phone': self.user_all_fields.phone,
                'first_name': self.user_all_fields.name,
                'last_name': self.user_all_fields.surname,
                'city': self.user_all_fields.city,
                'country': self.user_all_fields.country
            },
            'system': {
                'refresh_if_exists': 1,
                'partner_email': self.user_all_fields.partner_email,
                'multiple_offers': 0,
                'return_payment_link': 0,
                'return_deal_number': 0
            },
            'session': {
                'refresh_if_exists': 1,
                'partner_email': self.user_all_fields.partner_email
            },
            'deal': {
                'deal_number': self.deal_missing_fields.deal_id,
                'offer_code': self.deal_missing_fields.deal_number,
                'deal_is_paid': self.deal_missing_fields.paid
            }
        })
        self.assertEqual(len(result), 4)

    def test_create_params_for_deals_method_missing(self):
        """Тесты параметров импорта заказа с неполными данными"""
        result = create_params('deals', self.user_missing_fields, self.deal_missing_fields)

        self.assertEqual(result, {
            'user': {
                'email': self.user_missing_fields.email
            },
            'system': {
                'refresh_if_exists': 1,
                'multiple_offers': 0,
                'return_payment_link': 0,
                'return_deal_number': 0
            },
            'session': {
                'refresh_if_exists': 1
            },
            'deal': {
                'deal_number': self.deal_missing_fields.deal_id,
                'offer_code': self.deal_missing_fields.deal_number,
                'deal_is_paid': self.deal_missing_fields.paid
            }
        })
        self.assertEqual(len(result), 4)

