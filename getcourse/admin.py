from django.contrib import admin
from getcourse.models import *


class AuthGetCourseAdmin(admin.ModelAdmin):
    fieldsets = (('Подключение к API', {'fields': ('key', 'account_name')}),
                 ('Общие фильтры для экспорта', {
                  'fields': ('created_at', 'created_at_to')}),
                 ('Фильтры экспорта для пользователей и групп',
                  {'fields': ('status',)}),
                 ('Фильтры экспорта для групп', {
                  'fields': ('added_at', 'added_at_to')}),
                 ('Фильтры экспорта для заказов', {'fields': ('deal_status', 'deal_payed_at', 'deal_payed_at_to',
                                                              'deal_finished_at', 'deal_finished_at_to',
                                                              'deal_status_changed_at',
                                                              'deal_status_changed_at_to')}),
                 ('Фильтры экспорта для платежей', {'fields': ('payment_status', 'payment_status_changed_at',
                                                               'payment_status_changed_at_to')}),)


class UserAdmin(admin.ModelAdmin):
    fieldsets = (('Основные данные', {'fields': ('user_id',)}),
                 ('Данные пользователя', {'fields': ('name', 'surname', 'email', 'phone', 'vk_id', 'creat_date',
                                                     'last_activity', 'registration_type', 'birth_date',
                                                     'age', 'country', 'city', 'come_from', 'from_partner')}),
                 ('UTM', {'fields': ('utm_source', 'utm_medium', 'utm_campaign', 'utm_term', 'utm_content',
                                     'utm_group')}),
                 ('Партнер', {'fields': ('partner_id', 'manager_name', 'partner_name', 'partner_email')}))


class DealAdmin(admin.ModelAdmin):
    fieldsets = (('Основные данные', {'fields': ('deal_id', 'deal_number', 'product_title', 'deal_status',
                                                 'creat_date', 'payment_date')}),
                 ('Данные пользователя', {
                  'fields': ('user_id', 'user_name', 'user_email', 'user_phone', 'city')}),
                 ('Данные по платежу', {'fields': ('price', 'paid', 'payment_system_commission', 'received', 'tax',
                                                   'remaining_after_deduction', 'other_fees', 'earned', 'currency',
                                                   'payment_system', 'promo_code_used', 'promo_action')}),
                 ('Партнер', {'fields': ('partner_id', 'order_partner_id', 'order_partner_name', 'order_partner_email',
                                         'user_partner_id', 'user_partner_name', 'user_partner_email', 'partner_source',
                                         'partner_code', 'partner_session')}),
                 ('UTM', {'fields': ('utm_source', 'utm_medium', 'utm_campaign', 'utm_content', 'utm_term',
                                     'utm_group')}),
                 ('UTM Пользователя', {'fields': ('user_utm_source', 'user_utm_medium', 'user_utm_campaign',
                                                  'user_utm_content', 'user_utm_term', 'user_utm_group', 'user_gcpc')}))


class PaymentAdmin(admin.ModelAdmin):
    fieldsets = (('Основные данные', {'fields': ('payment_id', 'order', 'creat_date', 'payment_type',
                                                 'payment_status')}),
                 ('Данные пользователя', {'fields': ('user', 'email')}),
                 ('Информация по платежу', {'fields': ('payment_code', 'name', 'sum', 'commissions', 'received')}))


admin.site.register(AuthGetCourse, AuthGetCourseAdmin)
admin.site.register(GetCourseUser, UserAdmin)
admin.site.register(GetCourseGroup)
admin.site.register(GetCourseDeal, DealAdmin)
admin.site.register(GetCoursePayment, PaymentAdmin)
