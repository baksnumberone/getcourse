from time import sleep
from datetime import datetime
import requests
import re
from .models import *
from .serializers import *

# Данные для соответствия полей
USER_FIELDS = {'id': 'user_id',
               'Email': 'email',
               'Тип регистрации': 'registration_type',
               'Создан': 'creat_date',
               'Последняя активность': 'last_activity',
               'Имя': 'name',
               'Фамилия': 'surname',
               'Телефон': 'phone',
               'Дата рождения': 'birth_date',
               'Возраст': 'age',
               'Страна': 'country',
               'Город': 'city',
               'От партнера': 'from_partner',
               'Откуда пришел': 'come_from',
               'utm_source': 'utm_source',
               'utm_medium': 'utm_medium',
               'utm_campaign': 'utm_campaign',
               'utm_term': 'utm_term',
               'utm_content': 'utm_content',
               'utm_group': 'utm_group',
               'ID партнера': 'partner_id',
               'Email партнера': 'partner_email',
               'ФИО партнера': 'partner_name',
               'ФИО менеджера': 'manager_name',
               'VK-ID': 'vk_id'}
GROUP_FIELDS = {'id': 'group_id',
                'name': 'name',
                'last_added_at': 'last_added_at'}
DEAL_FIELDS = {'ID заказа': 'deal_id',
               'Номер': 'deal_number',
               'ID пользователя': 'user_id',
               'Пользователь': 'user_name',
               'Email': 'user_email',
               'Телефон': 'user_phone',
               'Дата создания': 'creat_date',
               'Дата оплаты': 'payment_date',
               'Title': 'product_title',
               'Статус': 'deal_status',
               'Стоимость, RUB': 'price',
               'Оплачено': 'paid',
               'Комиссия платежной системы': 'payment_system_commission',
               'Получено': 'received',
               'Налог': 'tax',
               'Осталось после вычета комиссии платежной системы и налога': 'remaining_after_deduction',
               'Другие комиссии': 'other_fees',
               'Заработано': 'earned',
               'Валюта': 'currency',
               'Менеджер': 'manager',
               'Город': 'city',
               'Платежная система': 'payment_system',
               'ID партнера': 'partner_id',
               'Использован промокод': 'promo_code_used',
               'Промоакция': 'promo_action',
               'ID партнера заказа': 'order_partner_id',
               'Email партнера заказа': 'order_partner_email',
               'ФИО партнера заказа': 'order_partner_name',
               'ID партнера пользователя': 'user_partner_id',
               'Email партнера пользователя': 'user_partner_email',
               'ФИО партнера пользователя': 'user_partner_name',
               'utm_source': 'utm_source',
               'utm_medium': 'utm_medium',
               'utm_campaign': 'utm_campaign',
               'utm_content': 'utm_content',
               'utm_term': 'utm_term',
               'utm_group': 'utm_group',
               'Партнерский источник': 'partner_source',
               'Партнерский код': 'partner_code',
               'Партнер (сессия)': 'partner_session',
               'user_utm_source': 'user_utm_source',
               'user_utm_medium': 'user_utm_medium',
               'user_utm_campaign': 'user_utm_campaign',
               'user_utm_content': 'user_utm_content',
               'user_utm_term': 'user_utm_term',
               'user_utm_group': 'user_utm_group',
               'user_gcpc': 'user_gcpc'}
PAYMENT_FIELDS = {'Номер': 'payment_id',
                  'Пользователь': 'user',
                  'Эл. почта': 'email',
                  'Заказ': 'payment_id',
                  'Дата создания': 'creat_date',
                  'Тип': 'payment_type',
                  'Статус': 'payment_status',
                  'Сумма': 'sum',
                  'Комиссии': 'commissions',
                  'Получено': 'received',
                  'Код платежа': 'payment_code',
                  'Название': 'name'}


# Создание параметров фильтрации для запроса получения данных
def creat_params(method, auth):
    params = {'key': auth.key}

    # Добавление общих параметров
    if auth.created_at:
        params['created_at[from]'] = auth.created_at.strftime('%Y-%m-%d')
    if auth.created_at_to:
        params['created_at[to]'] = auth.created_at_to.strftime('%Y-%m-%d')

    # Добавление параметров фильтра для экспорта пользователей
    if method == 'users':
        if auth.status:
            params['status'] = auth.status

    # Добавление параметров фильтра для экспорта групп
    if method == 'groups':
        if auth.status:
            params['status'] = auth.status
        if auth.added_at:
            params['added_at[from]'] = auth.added_at.strftime('%Y-%m-%d')
        if auth.added_at_to:
            params['added_at[to]'] = auth.added_at_to.strftime('%Y-%m-%d')

    # Добавление параметров фильтра для экспорта заказов
    if method == 'deals':
        if auth.deal_status:
            params['status'] = auth.deal_status
        if auth.deal_payed_at:
            params['payed_at[from]'] = auth.deal_payed_at.strftime('%Y-%m-%d')
        if auth.deal_payed_at_to:
            params['payed_at[to]'] = auth.deal_payed_at_to.strftime('%Y-%m-%d')
        if auth.deal_finished_at:
            params['finished_at[from]'] = auth.deal_finished_at.strftime(
                '%Y-%m-%d')
        if auth.deal_finished_at_to:
            params['finished_at[to]'] = auth.deal_finished_at_to.strftime(
                '%Y-%m-%d')
        if auth.deal_status_changed_at:
            params['status_changed_at[from]'] = auth.deal_status_changed_at.strftime(
                '%Y-%m-%d')
        if auth.deal_status_changed_at_to:
            params['status_changed_at[to]'] = auth.deal_status_changed_at_to.strftime(
                '%Y-%m-%d')

    # Добавление параметров фильтра для экспорта платежей
    if method == 'payments':
        if auth.payment_status:
            params['status'] = auth.payment_status
        if auth.payment_status_changed_at:
            params['status_changed_at[from]'] = auth.payment_status_changed_at.strftime(
                '%Y-%m-%d')
        if auth.payment_status_changed_at_to:
            params['status_changed_at[to]'] = auth.payment_status_changed_at_to.strftime(
                '%Y-%m-%d')

    return params


# Запрос на GetCourse для подготовки данных
def data_preparation(method, auth):
    url = f'https://{auth.account_name}.getcourse.ru/pl/api/account/{method}'

    params = creat_params(method, auth)
    while True:
        response = requests.get(url, params=params)
        data = response.json()

        if data['error'] and data['error_code'] == 905:
            sleep(1)
        elif data['error']:
            return {'error_message': data['error_message'], 'error_code': data['error_code']}, ''
        elif method == 'groups':
            return '', data['info']
        else:
            return '', data['info']['export_id']


# Получение подготовленных данных с GetCourse
def get_response(auth, export_id):
    url = f'https://{auth.account_name}.getcourse.ru/pl/api/account/exports/{export_id}?key={auth.key}'

    while True:
        response = requests.get(url)

        if not response.json()['error']:
            return response.json()
        else:
            sleep(1)


# Приведение полученых данных к масиву словарей
def pars_response_data(response_data):
    fields = response_data['fields']
    data = []

    for values in response_data['items']:
        data_for_object = {}

        for idx, value in enumerate(values):
            data_for_object[f'{fields[idx]}'] = value

        data.append(data_for_object)

    return data


# Обработка полученных значений и их приведение к нужным типам
def cast_value(method, key, value):
    pattern_data = re.compile(r"[0-9]{4}-[0-9]{2}-[0-9]{2}", re.IGNORECASE)
    pattern_data_time = re.compile(r"[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}(\.[0-9]{1,3})?",
                                   re.IGNORECASE)

    if pattern_data_time.match(value):
        return datetime.strptime(value, '%Y-%m-%d %H:%M:%S')
    elif pattern_data.match(value):
        return datetime.strptime(value, '%Y-%m-%d')
    elif method == 'deals' and key == 'Статус':
        for item in dict(GetCourseDeal.DEAL_STATUS).items():
            if item[1] == value:
                return item[0]
    elif method == 'payments' and key == 'Тип':
        for item in dict(GetCoursePayment.PAYMENT_TYPE).items():
            if item[1] == value:
                return item[0]
    elif method == 'payments' and key == 'Статус':
        for item in dict(GetCoursePayment.PAYMENT_STATUS).items():
            if item[1] == value:
                return item[0]
    else:
        return value


# Генерация data для создания объектов
def get_data_objects(method, fields_object, dict_value):
    data_objects = []

    for values in dict_value:
        data_object = {}

        for value in values.items():
            if value[0] in fields_object:
                if value[1]:
                    data_object[fields_object[value[0]]] = cast_value(
                        method, value[0], value[1])

        data_objects.append(data_object)

    return data_objects


# Получение связей полей
def get_fields(method):
    if method == 'users':
        return USER_FIELDS
    if method == 'deals':
        return DEAL_FIELDS
    if method == 'payments':
        return PAYMENT_FIELDS
    if method == 'groups':
        return GROUP_FIELDS


# Получение и преобразование данных с GetCourse
def get_data(method, auth):
    error_message, export_id = data_preparation(method, auth)
    if error_message:
        return error_message, ''

    response_data = get_response(auth, int(export_id))
    parsed_data = pars_response_data(response_data['info'])
    data = get_data_objects(method, get_fields(method), parsed_data)

    return '', data


# Экспорт пользователей с GetCourse
def get_users(auth):
    error_message, data_objects = get_data('users', auth)
    if error_message:
        return error_message

    user_created = 0
    user_updated = 0
    for data in data_objects:
        user, created = GetCourseUser.objects.update_or_create(
            user_id=data['user_id'], defaults=data)

        if created:
            user.save()
            user_created += 1
            continue
        user_updated += 1

    return {'users_created': user_created, 'users_updated': user_updated}


# Экспорт групп с GetCourse
def get_groups(auth):
    error_message, parsed_data = data_preparation('groups', auth)
    if error_message:
        return error_message

    data_objects = get_data_objects(
        'groups', get_fields('groups'), parsed_data)

    group_created = 0
    group_updated = 0
    for data in data_objects:
        group, created = GetCourseGroup.objects.update_or_create(
            group_id=data['group_id'], defaults=data)

        if created:
            group.save()
            group_created += 1
            continue
        group_updated += 1

    return {'groups_created': group_created, 'group_updated': group_updated}


# Экспорт заказов с GetCourse
def get_deals(auth):
    error_message, data_objects = get_data('deals', auth)
    if error_message:
        return error_message

    user_created = 0
    user_updated = 0
    for data in data_objects:
        deal, created = GetCourseDeal.objects.update_or_create(
            deal_id=data['deal_id'], defaults=data)

        if created:
            deal.save()
            user_created += 1
            continue
        user_updated += 1

    return {'orders_created': user_created, 'orders_updated': user_updated}


# Экспорт платежей с GetCourse
def get_payments(auth):
    error_message, data_objects = get_data('payments', auth)
    if error_message:
        return error_message

    payment_created = 0
    payment_updated = 0
    for data in data_objects:
        payment, created = GetCoursePayment.objects.update_or_create(
            payment_id=data['payment_id'], defaults=data)

        if created:
            payment.save()
            payment_created += 1
            continue
        payment_updated += 1

    return {'payments_created': payment_created, 'payments_updated': payment_updated}


# Экспорт данных с GetCourse Пользователи
def export_from_getcourse_user():
    for auth_account in AuthGetCourse.objects.all():
        if not auth_account.key or not auth_account.account_name:
            continue

        message = dict()
        message['users'] = get_users(auth_account)

        return message


# Экспорт данных с GetCourse Группы
def export_from_getcourse_groups():
    for auth_account in AuthGetCourse.objects.all():
        if not auth_account.key or not auth_account.account_name:
            continue

        message = dict()
        message['groups'] = get_groups(auth_account)

        return message


# Экспорт данных с GetCourse Заказы
def export_from_getcourse_deals():
    for auth_account in AuthGetCourse.objects.all():
        if not auth_account.key or not auth_account.account_name:
            continue

        message = dict()
        message['deals'] = get_deals(auth_account)

        return message


# Экспорт данных с GetCourse Платежи
def export_from_getcourse_payments():
    for auth_account in AuthGetCourse.objects.all():
        if not auth_account.key or not auth_account.account_name:
            continue

        message = dict()
        message['payments'] = get_payments(auth_account)

        return message
