from django.urls import path
from getcourse.views import *


urlpatterns = [
    path('export-getcourse-users', export_getcourse_users, name='getcourse-users'),
    path('export-getcourse-groups',
         export_getcourse_groups, name='getcourse-groups'),
    path('export-getcourse-deals', export_getcourse_deals, name='getcourse-deals'),
    path('export-getcourse-payments',
         export_getcourse_payments, name='getcourse-payments'),
    path('import-getcourse', import_getcourse, name='import_getcourse'),
]
