from rest_framework import serializers
from getcourse.models import GetCourseUser, GetCourseGroup, GetCourseDeal


class CreateUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = GetCourseUser
        fields = '__all__'


class CreateGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = GetCourseGroup
        fields = '__all__'


class CreateDealSerializer(serializers.ModelSerializer):
    class Meta:
        model = GetCourseDeal
        fields = '__all__'
