import base64
import json
import requests
from getcourse.models import AuthGetCourse, GetCourseUser, GetCourseDeal


def fill_user_params(user):
    """Заполнения параметров пользователя для импорта"""
    user_params = {}

    if user.email:
        user_params['email'] = user.email
    if user.phone:
        user_params['phone'] = user.phone
    if user.name:
        user_params['first_name'] = user.name
    if user.surname:
        user_params['last_name'] = user.surname
    if user.city:
        user_params['city'] = user.city
    if user.country:
        user_params['country'] = user.country

    return user_params


def fill_deal_params(deal):
    """Заполнения параметров заказа для импорта"""
    deal_params = {}

    if deal.deal_id:
        deal_params['deal_number'] = deal.deal_id
    if deal.deal_number:
        deal_params['offer_code'] = deal.deal_number
    if deal.product_title:
        deal_params['product_title'] = deal.product_title
    if deal.price:
        deal_params['deal_cost'] = deal.price
    if deal.deal_status:
        deal_params['deal_status'] = deal.deal_status
    if deal.paid:
        deal_params['deal_is_paid'] = deal.paid
    if deal.manager:
        deal_params['manager_email'] = deal.manager
    # TODO: Исправить
    #if deal.creat_date:
    #    deal_params['deal_created_at'] = deal.creat_date
    if deal.payment_date:
        deal_params['deal_finished_at'] = deal.payment_date
    if deal.order_partner_email:
        deal_params['partner_email'] = deal.order_partner_email
    if deal.currency:
        deal_params['deal_currency'] = deal.currency

    # "product_description": deal.product_description, #
    # "quantity": deal.quantity, #
    # "manager_email": "", #
    # "deal_comment": deal.comment,  #
    # "payment_type": deal.payment_type,
    # "payment_status": deal.sta, #

    return deal_params


def fill_user_system_params(user):
    """Заполнения системных параметров пользователя для импорта"""
    system_params = {'refresh_if_exists': 1}  # Обновлять ли существующего пользователя 1/0

    if user.partner_email:
        system_params['partner_email'] = user.partner_email

    return system_params


def fill_deal_system_params(user):
    """Заполнения системных параметров для импорта заказа"""
    system_params = {'refresh_if_exists': 1}  # Обновлять ли существующего пользователя 1/0

    if user.partner_email:
        system_params['partner_email'] = user.partner_email

    system_params['multiple_offers'] = 0  # Добавлять несколько предложений в заказ 1/0
    system_params['return_payment_link'] = 0  # Возвращать ссылку на оплату 1/0
    system_params['return_deal_number'] = 0  # Возвращать номер заказа 1/0

    return system_params


def fill_session_params(user):
    """Заполнения UTM параметров пользователя для импорта"""
    session_params = {}

    if user.utm_source:
        session_params['utm_source'] = user.utm_source
    if user.utm_medium:
        session_params['utm_medium'] = user.utm_medium
    if user.utm_campaign:
        session_params['utm_campaign'] = user.utm_campaign
    if user.utm_term:
        session_params['utm_term'] = user.utm_term
    if user.utm_content:
        session_params['utm_content'] = user.utm_content

    return session_params


def create_params(method, user, deal=None):
    """Подготовка данных для импорта пользователя"""
    user_params = fill_user_params(user)

    if method == 'users':
        system_params = fill_user_system_params(user)
        session_params = fill_session_params(user)
    elif method == 'deals':
        system_params = fill_deal_system_params(user)
        session_params = fill_user_system_params(user)
        deal_params = fill_deal_params(deal)

    params_dict = {
        'user': user_params,
        'system': system_params,
        'session': session_params
    }
    if method == 'deals':
        params_dict['deal'] = deal_params

    return params_dict


def requests_data(method, account, user, deal=None):
    """Импорт данных на GetCourse"""
    # Заполняем параметры для импорта
    if method == 'users':
        params = create_params(method, user)
    elif method == 'deals':
        params = create_params(method, user, deal)

    # Кодируем параметры пользователя в формат base64
    user_params_encoded = base64.b64encode(json.dumps(params).encode()).decode()

    # Отправляем запрос на добавление пользователя в API GetCourse
    url = f"https://{account.account_name}.getcourse.ru/pl/api/{method}"
    data = {"action": "add", "key": account.key, "params": user_params_encoded}

    return requests.post(url, data=data)


def import_to_getcourse():
    """Импорт данных на площадку GetCourse"""
    users = GetCourseUser.objects.all()
    deals = GetCourseDeal.objects.all()

    for account in AuthGetCourse.objects.all():
        if not account.key or not account.account_name:
            continue

        for user in users:
            # Email обязательный параметр для импорта
            if not user.email:
                continue

            # Отправляем запрос на добавление пользователя в API GetCourse
            response = requests_data('users', account, user)
            if response.status_code == 200:
                print(f"Пользователь {user.email} был успешно импортирован на GetCourse.")
            else:
                print(f"Ошибка импорта пользователя {user.email} на GetCourse. Error: {response.text}")

        for deal in deals:
            # User_id обязательный параметр для импорта
            if not GetCourseUser.objects.filter(user_id=deal.user_id).exists():
                continue
            # offer_code или product_title должны быть заполнены для импорта
            if not deal.deal_number and not deal.product_title:
                continue

            # Отправляем запрос на добавление заказа в API GetCourse
            user = GetCourseUser.objects.get(user_id=deal.user_id)
            response = requests_data('deals', account, user, deal)
            if response.status_code == 200:
                print(f'Заказ №{deal.deal_id} был успешно импортирован на GetCourse.')
            else:
                print(f'Ошибка импорта заказа №{deal.deal_id} на GetCourse. Error: {response.text}')
