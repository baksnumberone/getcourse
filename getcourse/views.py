from rest_framework.response import Response
from rest_framework import generics
from .serializers import *
from .getcourse import *
from rest_framework.decorators import api_view
import requests
import datetime as dt
from rest_framework import status
from .getcourse_import import *


class CreateUserView(generics.CreateAPIView):
    serializer_class = CreateUserSerializer
    queryset = GetCourseUser


@api_view(['GET'])
def export_getcourse_users(request):
    message = export_from_getcourse_user()
    return Response({'data': message})


@api_view(['GET'])
def export_getcourse_groups(request):
    message = export_from_getcourse_groups()
    return Response({'data': message})


@api_view(['GET'])
def export_getcourse_deals(request):
    message = export_from_getcourse_deals()
    return Response({'data': message})


@api_view(['GET'])
def export_getcourse_payments(request):
    message = export_from_getcourse_payments()
    return Response({'data': message})


@api_view(['GET'])
def import_getcourse(request):
    #message = import_users_to_getcourse()
    #import_deals_to_getcourse()
    import_to_getcourse()
    return Response({'data': ''})

