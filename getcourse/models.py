from django.db import models


class AuthGetCourse(models.Model):
    STATUSES = [
        ('active', 'Активный'),
        ('in_base', 'В базе')
    ]
    DEAL_STATUS = [
        ('new', 'Новый'),
        ('payed', 'Завершен'),
        ('cancelled', 'Отменен'),
        ('in_work', 'В работе'),
        ('payment_waiting', 'Ожидаем оплаты'),
        ('part_payed', 'Частично оплачен'),
        ('waiting_for_return', 'Ожидание возврата оплаты'),
        ('not_confirmed', 'Ложный'),
        ('pending', 'Ожидаем возврата'),
        ('False', 'Ложный')
    ]
    PAYMENT_STATUS = [
        ('expected', 'Ожидается'),
        ('accepted', 'Получен'),
        ('returned', 'Возвращен'),
        ('tobalance', 'Пополнение баланса'),
        ('frombalance', 'Списание с баланса'),
        ('returned_to_balance', 'Начислен на депозит')
    ]

    key = models.CharField(verbose_name='Секретный ключ',
                           max_length=400,
                           blank=True)
    account_name = models.CharField(verbose_name='Аккаунт',
                                    max_length=200,
                                    blank=True)

    # Общие данные экспорта для всех записей
    created_at = models.DateField(verbose_name='Дата создания с',
                                  null=True,
                                  blank=True)
    created_at_to = models.DateField(verbose_name='Дата создания по',
                                     null=True,
                                     blank=True)

    # Данные экспорта для пользователей и групп
    status = models.CharField(verbose_name='Статус',
                              max_length=7,
                              choices=STATUSES,
                              null=True,
                              blank=True)

    # Данные экспорта для групп
    added_at = models.DateField(verbose_name='Дата добавления в группу c',
                                null=True,
                                blank=True)
    added_at_to = models.DateField(verbose_name='Дата добавления в группу по',
                                   null=True,
                                   blank=True)

    # Данные экспорта для заказов
    deal_status = models.CharField(verbose_name='Статус сделки',
                                   max_length=19,
                                   choices=DEAL_STATUS,
                                   null=True,
                                   blank=True)
    deal_payed_at = models.DateField(verbose_name='Дата оплаты заказа c',
                                     null=True,
                                     blank=True)
    deal_payed_at_to = models.DateField(verbose_name='Дата оплаты заказа по',
                                        null=True,
                                        blank=True)
    deal_finished_at = models.DateField(verbose_name='Дата завершения заказа c',
                                        null=True,
                                        blank=True)
    deal_finished_at_to = models.DateField(verbose_name='Дата завершения заказа по',
                                           null=True,
                                           blank=True)
    deal_status_changed_at = models.DateField(verbose_name='Дата изменения статуса заказа c',
                                              null=True,
                                              blank=True)
    deal_status_changed_at_to = models.DateField(verbose_name='Дата изменения статуса заказа по',
                                                 null=True,
                                                 blank=True)

    # Данные экспорта для платежей
    payment_status = models.CharField(verbose_name='Статус платежа',
                                      max_length=20,
                                      choices=PAYMENT_STATUS,
                                      null=True,
                                      blank=True)
    payment_status_changed_at = models.DateField(verbose_name='Дата изменения статуса платежа c',
                                                 null=True,
                                                 blank=True)
    payment_status_changed_at_to = models.DateField(verbose_name='Дата изменения статуса платежа по',
                                                    null=True,
                                                    blank=True)

    def __str__(self):
        return self.account_name

    class Meta:
        verbose_name = 'Данные экспорта'
        verbose_name_plural = 'Данные экспорта'


class GetCourseUser(models.Model):
    user_id = models.CharField(verbose_name='ID Пользователя',
                               unique=True,
                               max_length=200,
                               blank=True)
    email = models.CharField(verbose_name='Email',
                             max_length=300,
                             blank=True)
    registration_type = models.CharField(verbose_name='Тип регистрации',
                                         max_length=300,
                                         blank=True)
    creat_date = models.DateTimeField(verbose_name='Дата создания',
                                      null=True,
                                      blank=True)
    last_activity = models.DateTimeField(verbose_name='Последняя активность',
                                         null=True,
                                         blank=True)
    name = models.CharField(verbose_name='Имя',
                            max_length=200,
                            blank=True)
    surname = models.CharField(verbose_name='Фамилия',
                               max_length=200,
                               blank=True)
    phone = models.CharField(verbose_name='Телефон',
                             max_length=200,
                             blank=True)
    birth_date = models.DateField(verbose_name='День рождения',
                                  null=True,
                                  blank=True)
    age = models.CharField(verbose_name='Возраст',
                           max_length=200,
                           blank=True)
    country = models.CharField(verbose_name='Страна',
                               max_length=200,
                               blank=True)
    city = models.CharField(verbose_name='Город',
                            max_length=200,
                            blank=True)
    from_partner = models.CharField(verbose_name='От партнера',
                                    max_length=200,
                                    blank=True)
    come_from = models.CharField(verbose_name='Откуда пришел',
                                 max_length=200,
                                 blank=True)
    utm_source = models.CharField(verbose_name='utm_source',
                                  max_length=200,
                                  blank=True)
    utm_medium = models.CharField(verbose_name='utm_medium',
                                  max_length=200,
                                  blank=True)
    utm_campaign = models.CharField(verbose_name='utm_campaign',
                                    max_length=200,
                                    blank=True)
    utm_term = models.CharField(verbose_name='utm_term',
                                max_length=200,
                                blank=True)
    utm_content = models.CharField(verbose_name='utm_content',
                                   max_length=200,
                                   blank=True)
    utm_group = models.CharField(verbose_name='utm_group',
                                 max_length=200,
                                 blank=True)
    partner_id = models.CharField(verbose_name='ID Партнера',
                                  max_length=200,
                                  blank=True)
    partner_email = models.CharField(verbose_name='Email партнера',
                                     max_length=300,
                                     blank=True)
    partner_name = models.CharField(verbose_name='Имя партнера',
                                    max_length=200,
                                    blank=True)
    manager_name = models.CharField(verbose_name='Имя менеджера',
                                    max_length=200,
                                    blank=True)
    vk_id = models.CharField(verbose_name='VK_ID',
                             max_length=200,
                             blank=True)

    def __str__(self):
        return self.email

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'


class GetCourseGroup(models.Model):
    group_id = models.CharField(verbose_name='ID группы',
                                unique=True,
                                max_length=200,
                                blank=True)
    name = models.CharField(verbose_name='Название группы',
                            max_length=200,
                            blank=True)
    last_added_at = models.DateTimeField(verbose_name='Дата добавления последнего пользователя',
                                         null=True,
                                         blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Группа'
        verbose_name_plural = 'Группы'


class GetCourseDeal(models.Model):
    DEAL_STATUS = [
        ('new', 'Новый'),
        ('payed', 'Завершен'),
        ('cancelled', 'Отменен'),
        ('in_work', 'В работе'),
        ('payment_waiting', 'Ожидаем оплаты'),
        ('part_payed', 'Частично оплачен'),
        ('waiting_for_return', 'Ожидание возврата оплаты'),
        ('not_confirmed', 'Ложный'),
        ('pending', 'Ожидаем возврата'),
        ('False', 'Ложный')
    ]
    DEAL_PAID = [
        ('Yes', 'Да'),
        ('No', 'Нет')
    ]
    PAYMENT_STATUS = [
        ('expected', 'Ожидается'),
        ('accepted', 'Получен'),
        ('returned', 'Возвращен'),
        ('tobalance', 'Пополнение баланса'),
        ('frombalance', 'Списание с баланса'),
        ('returned_to_balance', 'Начислен на депозит')
    ]

    deal_id = models.CharField(verbose_name='ID заказа',
                               unique=True,
                               max_length=200,
                               blank=True)
    deal_number = models.CharField(verbose_name='Номер заказа',
                                   max_length=200,
                                   blank=True)
    user_id = models.CharField(verbose_name='ID пользователя',
                               max_length=200,
                               blank=True)
    user_name = models.CharField(verbose_name='Пользователь',
                                 max_length=200,
                                 blank=True)
    user_email = models.CharField(verbose_name='Email',
                                  max_length=300,
                                  blank=True)
    user_phone = models.CharField(verbose_name='Телефон',
                                  max_length=300,
                                  blank=True)
    creat_date = models.DateTimeField(verbose_name='Дата создания',
                                      null=True,
                                      blank=True)
    payment_date = models.DateTimeField(verbose_name='Дата оплаты',
                                        null=True,
                                        blank=True)
    product_title = models.CharField(verbose_name='Наименование предложения',
                                     max_length=200,
                                     blank=True)
    deal_status = models.CharField(verbose_name='Статус заказа',
                                   max_length=19,
                                   choices=DEAL_STATUS,
                                   null=True,
                                   blank=True)
    price = models.CharField(verbose_name='Стоимость, RUB',
                             max_length=200,
                             blank=True)
    paid = models.CharField(verbose_name='Оплачено',
                            max_length=200,
                            choices=DEAL_PAID,
                            null=True,
                            blank=True)
    payment_system_commission = models.CharField(verbose_name='Комиссия платежной системы',
                                                 max_length=200,
                                                 blank=True)
    received = models.CharField(verbose_name='Получено',
                                max_length=200,
                                blank=True)
    tax = models.CharField(verbose_name='Налог',
                           max_length=200,
                           blank=True)
    remaining_after_deduction = models.CharField(verbose_name='Осталось после вычета',
                                                 max_length=200,
                                                 blank=True)
    other_fees = models.CharField(verbose_name='Другие комиссии',
                                  max_length=200,
                                  blank=True)
    earned = models.CharField(verbose_name='Заработано',
                              max_length=200,
                              blank=True)
    currency = models.CharField(verbose_name='Валюта',
                                max_length=200,
                                blank=True)
    manager = models.CharField(verbose_name='Менеджер',
                               max_length=200,
                               blank=True)
    city = models.CharField(verbose_name='Город',
                            max_length=200,
                            blank=True)
    payment_system = models.CharField(verbose_name='Платежная система',
                                      max_length=200,
                                      blank=True)
    partner_id = models.CharField(verbose_name='ID партнера',
                                  max_length=200,
                                  blank=True)
    promo_code_used = models.CharField(verbose_name='Использован промокод',
                                       max_length=200,
                                       blank=True)
    promo_action = models.CharField(verbose_name='Промоакция',
                                    max_length=200,
                                    blank=True)
    order_partner_id = models.CharField(verbose_name='ID партнера заказа',
                                        max_length=200,
                                        blank=True)
    order_partner_email = models.CharField(verbose_name='Email партнера заказа',
                                           max_length=200,
                                           blank=True)
    order_partner_name = models.CharField(verbose_name='ФИО партнера заказа',
                                          max_length=200,
                                          blank=True)
    user_partner_id = models.CharField(verbose_name='ID партнера пользователя',
                                       max_length=200,
                                       blank=True)
    user_partner_email = models.CharField(verbose_name='Email партнера пользователя',
                                          max_length=200,
                                          blank=True)
    user_partner_name = models.CharField(verbose_name='ФИО партнера пользователя',
                                         max_length=200,
                                         blank=True)
    utm_source = models.CharField(verbose_name='utm_source',
                                  max_length=200,
                                  blank=True)
    utm_medium = models.CharField(verbose_name='utm_medium',
                                  max_length=200,
                                  blank=True)
    utm_campaign = models.CharField(verbose_name='utm_campaign',
                                    max_length=200,
                                    blank=True)
    utm_content = models.CharField(verbose_name='utm_content',
                                   max_length=200,
                                   blank=True)
    utm_term = models.CharField(verbose_name='utm_term',
                                max_length=200,
                                blank=True)
    utm_group = models.CharField(verbose_name='utm_group',
                                 max_length=200,
                                 blank=True)
    partner_source = models.CharField(verbose_name='Партнерский источник',
                                      max_length=200,
                                      blank=True)
    partner_code = models.CharField(verbose_name='Партнерский код',
                                    max_length=200,
                                    blank=True)
    partner_session = models.CharField(verbose_name='Партнер (сессия)',
                                       max_length=200,
                                       blank=True)
    user_utm_source = models.CharField(verbose_name='user_utm_source',
                                       max_length=200,
                                       blank=True)
    user_utm_medium = models.CharField(verbose_name='user_utm_medium',
                                       max_length=200,
                                       blank=True)
    user_utm_campaign = models.CharField(verbose_name='user_utm_campaign',
                                         max_length=200,
                                         blank=True)
    user_utm_content = models.CharField(verbose_name='user_utm_content',
                                        max_length=200,
                                        blank=True)
    user_utm_term = models.CharField(verbose_name='user_utm_term',
                                     max_length=200,
                                     blank=True)
    user_utm_group = models.CharField(verbose_name='user_utm_group',
                                      max_length=200,
                                      blank=True)
    user_gcpc = models.CharField(verbose_name='user_gcpc',
                                 max_length=200,
                                 blank=True)

    def __str__(self):
        return self.product_title

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'


class GetCoursePayment(models.Model):
    PAYMENT_TYPE = [
        ('cashless', 'Cashless'),
        ('Z_payment', 'Z-payment'),
        ('cash', 'Cash'),
        ('card_terminal', 'Bank card by terminal'),
        ('webmoney', 'Webmoney'),
        ('your_money', 'ЮMoney'),
        ('your_kassa', 'ЮKassa'),
        ('card', 'Card'),
        ('paypal', 'PayPal'),
        ('qiwi', 'Qiwi'),
        ('sber', 'Sber'),
        ('two_co', '2CO'),
        ('rbk_money', 'RBK Money'),
        ('rbk_money2', 'RBK.Money'),
        ('perfect_money', 'Perfect Money'),
        ('robokassa', 'Robokassa'),
        ('internal_balance', 'Internal balance'),
        ('quick_transfer_systems', 'Quick transfer systems'),
        ('other', 'Other'),
        ('bonus_account', 'Bonus account'),
        ('wallet_one', 'WalletOne'),
        ('cloud_payments', 'CloudPayments'),
        ('cloud_payments_kz', 'CloudPaymentsKz'),
        ('pay_any_way', 'PayAnyWay'),
        ('sberbank', 'Sberbank'),
        ('tinkoff_bank', 'Tinkoff Bank'),
        ('fondy', 'Fondy'),
        ('justclick', 'Justclick'),
        ('way_for_pay', 'WayForPay'),
        ('tinkoff_credit', 'Тинькофф Кредит'),
        ('stripe', 'Stripe'),
        ('alfabank', 'Alfabank'),
        ('hutki_grosh', 'Хуткi Грош'),
        ('interkassa', 'Interkassa'),
        ('invoice_for_payment', 'Счет на оплату'),
        ('bank_receipt', 'Квитанция в банк'),
        ('qiwi_kassa', 'QIWI Kassa'),
        ('two_checkout', '2Checkout'),
        ('prodamus', 'Prodamus'),
        ('ebanx', 'EBANX'),
        ('pag_seguro', 'PagSeguro'),
        ('pagar_me', 'Pagar.me'),
        ('pelecard', 'Pelecard'),
        ('mandarin_life', 'Mandarin.life'),
        ('be_paid', 'BePaid'),
        ('pag_brasil', 'PagBrasil'),
        ('payu_russia', 'PayU Russia'),
        ('payu_latam', 'PayU Latam'),
        ('payu_poland', 'PayU Poland'),
        ('atlant_payments', 'Atlant Payments'),
        ('hotmart', 'Hotmart'),
        ('payu_india', 'PayU India'),
        ('digistore24', 'Digistore24'),
        ('jetpay', 'JetPay'),
        ('poscredit', 'Poscredit (рассрочки)'),
        ('always_yes', 'Всегда.Да'),
        ('appex', 'Appex'),
        ('mercado_pago', 'Mercado Pago'),
        ('divide', 'Подели'),
        ('intellect_money', 'IntellectMoney'),
        ('doku_jokul', 'DOKU Jokul'),
        ('unlimint', 'Unlimint'),
        ('share', 'Долями'),
        ('buy_with_sber', 'Покупай со Сбером')
    ]
    PAYMENT_STATUS = [
        ('expected', 'Ожидается'),
        ('accepted', 'Получен'),
        ('returned', 'Возвращен'),
        ('tobalance', 'Пополнение баланса'),
        ('frombalance', 'Списание с баланса'),
        ('returned_to_balance', 'Начислен на депозит')
    ]

    payment_id = models.CharField(verbose_name='Номер',
                                  unique=True,
                                  max_length=200,
                                  blank=True)
    user = models.CharField(verbose_name='Пользователь',
                            max_length=200,
                            blank=True)
    email = models.CharField(verbose_name='Email',
                             max_length=200,
                             blank=True)
    order = models.CharField(verbose_name='Заказ',
                             max_length=200,
                             blank=True)
    creat_date = models.DateTimeField(verbose_name='Дата создания',
                                      null=True,
                                      blank=True)
    payment_type = models.CharField(verbose_name='Тип платежа',
                                    max_length=22,
                                    choices=PAYMENT_TYPE,
                                    null=True,
                                    blank=True)
    payment_status = models.CharField(verbose_name='Статус платежа',
                                      max_length=19,
                                      choices=PAYMENT_STATUS,
                                      null=True,
                                      blank=True)
    sum = models.CharField(verbose_name='Сумма',
                           max_length=200,
                           blank=True)
    commissions = models.CharField(verbose_name='Комиссии',
                                   max_length=200,
                                   blank=True)
    received = models.CharField(verbose_name='Получено',
                                max_length=200,
                                blank=True)
    payment_code = models.CharField(verbose_name='Код платежа',
                                    max_length=200,
                                    blank=True)
    name = models.CharField(verbose_name='Название',
                            max_length=200,
                            blank=True)

    def __str__(self):
        return self.payment_id

    class Meta:
        verbose_name = 'Платеж'
        verbose_name_plural = 'Платежи'
